# Cycle

A cycle data analyser which parses HRM files created by a Polar cycle machine and produces useful statistics for athletes. 

## Extended Features and Functionality

All HRM Data is displayed on chart. Summary data is displayed in panel on right hand side.

###Calendar  
* Cycle (HRM) Data can be loaded by directory and displayed by date through ```File > Import Cycle Directory```
* Bolded dates show HRM files which can loaded by clicking on them

###Selectable Data  
* Data can be selected automatically through the ```Edit > Auto Interval Detection Settings > Apply```
* Or turned on initially through ```Data > Intervals``` if no Manual intervals have been made
* Data for each interval is displayed in the right panel
* Selected Data can then be expanded or reduced by holding control and holding down left-click at the TOP of the interval lines whilst dragging it into a new position
* Manual interval lines can be added at any point by double-clicking on the graph
* Manual intervals override Auto intervals
* The panel lets you cycle through interval data and summary data
* When intervals are set and toggled on through ```Data > Intervals``` the "Summary" section shows totals, averages and metrics for data within intervals only
* When intervals are toggled off, "Summary" shows totals, averages and metrics for whole data read

###Auto Interval Detection
* Accessed through ```Edit > Auto Interval Detection Settings```
* Five variables are used for this process:
* ```threshold``` - the lowest value for which peak detection will begin at
* ```tolerance``` - the variance above or below the start_peak before an end_peak is flagged
* ```least_interval_size``` - ignores flagged end-peak if the number of records between start_peak and end_peak is less than this value
* Between ```end_peak_value_minus_tolerance_start``` and ```end_peak_value_minus_tolerance_end``` any flagged end_peaks which are not smaller than the tolerance + the value before the end_peak are ignored as end peaks (N.B. Helps detect large power spikes in brief intervals. In example HRM file the last 5 intervals are detected this way)

###Metrics    
* Normalised Power (NP) - worked out as ```((300 x 150)^4 + (400 x 200)^4)^0.25``` where each interval is (seconds x watts) ^ 4 and the sum is then root 4
* Intensity Factor (IF) - worked out as ```300/320 * 100 = 93.75%```. NP / Functional Threshold Power (FTP) = IF
* Training Test Score (TSS) - worked out as (NP / FTP )^2 * hours * 100

###Power and Heart Rate Zones
* [Based on Coggan's Power and MHR Zones](http://home.trainingpeaks.com/blog/article/power-training-levels)
* Uses HRM Data MHR but allows user to override in ```Edit > User Settings```
* Uses FTP from user input in ```User Settings```

## Basic Functionality

###Reads in Header information    
* Time and Date of training session
* Interval (known as frequency between each data record in seconds)

###Reads HRM Data    
* Speed (km/h or mph)
* Cadence (rpm)
* Altitude (ft)
* Heart Rate (bpm)
* Power (watts)
* Pedalling Index (%)
* Left Right Balance (%)

###Summary Data    
* Total Distance
* Average Speed
* Maximum Speed
* Average Heart Rate
* Maximum Heart Rate
* Minimum Heart Rate
* Average Power
* Maximum Power
* Average Altitude
* Maximum Altitude    

###Chart Display
* ZedGraph - Linegraph, Piechart

#### Classes

```HRMFileReader``` - Parses HRM Data for HRMObjects to be created from    
```HRMObject``` - Holds a HRM Data subset (i.e. altitude, heart rate, cadence ...)    
```HRMObjectSpeed``` - Derived Class holds Speed HRM Data subset only    
```HRMObjectPower``` - Derived Class holds Power HRM Data subset only    
```HRMObjectCollection``` - Collection of HRMObjects    
```ZGraph``` - Handles interaction with ZedGraphControl    

#### Methods

#####```HRMFileReader```
* ```HRMFileReader()``` - Constructor opens HRM file through file dialog
* ```read_from_file()``` - Called by constructor, populates arrays with parsed data

#####```HRMObject```
* ```calcMin()``` - Returns minimum value between two points
* ```calcMax()``` - Returns maximum value between two points
* ```calcTotal()``` - Returns total sum between two points
* ```calcAvg()``` - Returns ```calcTotal``` divided by number of records between two points

#####```HRMObjectSpeed```
* ```calcDistance()``` - Returns distance based on calcAvg() / 60 / 60 * records * frequency

#####```HRMObjectPower```
* ```calcNormalisedPower()``` - Returns normalised power
* ```calcIntensityFactor()``` - Returns Intensity factor
* ```calcTSS()``` - Returns Training Stress Score
* ```createIntervals()``` - Returns List of int[2] (start, end) points of Intervals


#####```HRMObjectCollection```
* ```getObject()``` - Returns a single HRMObject of index (i) from the collection
* ```add()``` - Adds a HRMObject to the collection
* ```remove()``` - Removes a HRMObject from collection by (name)
* ```getCount()``` - Returns total number of HRMObjects in collection
* ```clear()``` Removes all HRMObjects from collection

#####```ZGraph```
* ```plotLineGraph()``` - Plots a ZedGraph Linegraph using a HRMObjectCollection and Interval List
* ```createZonesForLineGraph()``` - Draws bands on ZedGraph representing Coggan's Power and Heart Rate Zones
* ```clearZonesForLineGraph()``` - Removes Power and Heart Rate Zone bands
* ```plotPieChart()``` - Plots a ZedGraph piechart depicting L/R Balance for Summary Data
* ```createIntervals()``` - Draws manual interval lines onto ZedGraph chart area
* ```clearPane()``` - Reset Zedgraph chart area
* ```ZGraphControl()``` - Enables scroll bars, re-calculates axis ranges and redraws chart
* ```ConvertPointToHRMObjectIndex()``` - Converts a LineItem interval line to a HRMObject.data[index]
	
#### Tests    

##### Solution includes Test Project CycleTest
* ```ZGraph_plot_shouldThrowExceptionWithNoHRMObjectsInCollection()```
* ```ObjectCollection_add_getObject_shouldAddAndGetHRMObjectsToFromCollection()```
* ```HRMParser_readFromFile_shouldUpdateClassPropertyValues()```
* ```HRMParser_readFromFile_shouldThrowExceptionWithBlankFileName()```
* ```HRMObject_checkOptionalParamsMatchDefault()```
* ```HRMObject_checkChildClassInheritsParentClassMethod()```
* ```HRMObject_checkNumberFormatting()```