﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Forms;
using System.IO;
using Cycle;
using System.Drawing;
using System.Collections.Generic;

namespace CycleTests
{
    [TestClass]
    public class HRMObject_Test
    {
        [TestMethod]
        public void HRMObject_checkOptionalParamsMatchDefault()
        {
            // Create Dummy HRM Data 
            int[] hrmDummyData = new int[0];

            // Create Dummy HRM Objects
            HRMObject speedHRMObject = new HRMObject("Speed (km/h)", hrmDummyData, Color.Red);

            // Add HRM Objects to HRMObjectCollection
            HRMObjectCollection hrmObjectCollection = new HRMObjectCollection();
            hrmObjectCollection.add(speedHRMObject);

            // Null Variables
            List<int[]> interval = null;
            bool intervals_enabled = false;
            int interval_index = -1;

            Assert.AreEqual(hrmObjectCollection.getObject(0).calcMax(),
                hrmObjectCollection.getObject(0).calcMax(interval, intervals_enabled, interval_index));
        }

        [TestMethod]
        public void HRMObject_checkChildClassInheritsParentClassMethod()
        {
            // Create Dummy HRM Data 
            int[] hrmDummyData = new int[0];

            // Create Dummy HRM Objects
            HRMObjectSpeed speed = new HRMObjectSpeed("Speed (km/h)", hrmDummyData, Color.Red);

            Assert.IsNotNull(speed.calcMin());
        }

        [TestMethod]
        public void HRMObject_checkNumberFormatting()
        {
            // Create Dummy HRM Data 
            int[] hrmDummyData = new int[0];

            // Create Dummy HRM Objects
            HRMObjectPower power = new HRMObjectPower("Power (watts)", hrmDummyData, Color.Violet);

            // Dummy Varibles for Classes
            List<int[]> interval = null;
            bool intervals_enabled = false;
            double ftp = 0.0;
            double frequency = 0.0;
            int threshold = 0;
            int tolerance = 0;
            int least_interval_size = 0;
            int end_peak_value_minus_tolerance_start = 0;
            int end_peak_value_minus_tolerance_end = 0;

            // Base Class
            Assert.IsInstanceOfType(power.calcMin(), typeof(int));
            Assert.IsInstanceOfType(power.calcMax(), typeof(int));
            Assert.IsInstanceOfType(power.calcTotal(), typeof(int));
            Assert.IsInstanceOfType(power.calcAvg(), typeof(int));
            Assert.IsInstanceOfType(power.createIntervals(threshold, tolerance, least_interval_size, end_peak_value_minus_tolerance_start, end_peak_value_minus_tolerance_end), typeof(List<int[]>));

            // Derived Class
            Assert.IsInstanceOfType(power.calcNormalisedPower(interval, intervals_enabled), typeof(int));
            Assert.IsInstanceOfType(power.calcIntensityFactor(interval, intervals_enabled, ftp), typeof(int));
            Assert.IsInstanceOfType(power.calcTSS(interval, intervals_enabled, ftp, frequency), typeof(int));
        }
    }
}
