﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Forms;
using System.IO;
using Cycle;

namespace CycleTests
{
    [TestClass]
    public class HRMParse_Test
    {
        [TestMethod]
        public void HRMParser_readFromFile_shouldUpdateClassPropertyValues()
        {
            // Read HRM File with Correct Data
            HRMFileReader hrmparser = new HRMFileReader();

            // Check Arrays in HRMParser are updated by read_from_file
            Assert.IsNotNull(hrmparser.Speed);
            Assert.IsNotNull(hrmparser.Cadence);
            Assert.IsNotNull(hrmparser.Altitude);
            Assert.IsNotNull(hrmparser.Power);
            Assert.IsNotNull(hrmparser.HeartRate);   
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void HRMParser_readFromFile_shouldThrowExceptionWithBlankFileName()
        {
            // Taken from HRMParser read_from_file, checks trying to parse
            // a file with no filename specified
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Open HRM File";
            dialog.Filter = "HRM files|*.hrm";
            dialog.InitialDirectory = @"C:\";
            string[] line = File.ReadAllLines(dialog.FileName);
        }
    }
}