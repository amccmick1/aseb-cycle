﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cycle;

namespace CycleTests
{
    [TestClass]
    public class ObjectCollection_Test
    {

        [TestMethod]
        public void ObjectCollection_add_getObject_shouldAddAndGetHRMObjectsToFromCollection()
        {
            // Create Dummy HRM Data 
            int[] hrmDummyData = new int[0];

            // Create Dummy HRM Objects
            HRMObject speedHRMObject = new HRMObject("Speed (km/h)", hrmDummyData, Color.Red);
            HRMObject cadenceHRMObject = new HRMObject("Cadence (rpm)", hrmDummyData, Color.Blue);
            HRMObject altitudeHRMObject = new HRMObject("Altitude (ft)", hrmDummyData, Color.Green);
            HRMObject powerHRMObject = new HRMObject("Power (watts)", hrmDummyData, Color.Violet);
            HRMObject heartRateHRMObject = new HRMObject("Heart Rate (bpm)", hrmDummyData, Color.Orange);

            // Add HRM Objects to HRMObjectCollection
            HRMObjectCollection hrmObjectCollection = new HRMObjectCollection();
            hrmObjectCollection.add(speedHRMObject);
            hrmObjectCollection.add(cadenceHRMObject);
            hrmObjectCollection.add(altitudeHRMObject);
            hrmObjectCollection.add(powerHRMObject);
            hrmObjectCollection.add(heartRateHRMObject);

            // Check Objects are Added and are the Same with getObject
            Assert.AreSame(hrmObjectCollection.getObject(0), speedHRMObject);
            Assert.AreSame(hrmObjectCollection.getObject(1), cadenceHRMObject);
            Assert.AreSame(hrmObjectCollection.getObject(2), altitudeHRMObject);
            Assert.AreSame(hrmObjectCollection.getObject(3), powerHRMObject);
            Assert.AreSame(hrmObjectCollection.getObject(4), heartRateHRMObject);
        }
    }
}
