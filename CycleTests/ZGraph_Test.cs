﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cycle;
using ZedGraph;

namespace CycleTests
{
    [TestClass]
    public class ZGraph_Test
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ZGraph_plot_shouldThrowExceptionWithNoHRMObjectsInCollection()
        {            
            // Dummy Variables
            ZedGraphControl zgc = new ZedGraphControl();
            DateTime startDateTime = new DateTime();
            double duration = 0;
            
            // Empty HRMObjectCollection
            HRMObjectCollection hrmObjectCollection = new HRMObjectCollection();

            // Plot ZGraph using Empty HRMObjectCollection
            ZGraph zgraph = new ZGraph(zgc, startDateTime, duration);
            zgraph.plotLineGraph(zgc, hrmObjectCollection, 1);
        }
    }
}
