﻿namespace Cycle
{
    partial class User
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.applyBtn = new System.Windows.Forms.Button();
            this.maxHeartRate = new System.Windows.Forms.TextBox();
            this.functionalThresholdPower = new System.Windows.Forms.TextBox();
            this.functionalThresholdPowerLbl = new System.Windows.Forms.Label();
            this.maxHeartRateLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // applyBtn
            // 
            this.applyBtn.Location = new System.Drawing.Point(37, 175);
            this.applyBtn.Name = "applyBtn";
            this.applyBtn.Size = new System.Drawing.Size(75, 23);
            this.applyBtn.TabIndex = 0;
            this.applyBtn.Text = "Apply";
            this.applyBtn.UseVisualStyleBackColor = true;
            this.applyBtn.Click += new System.EventHandler(this.applyBtn_Click);
            // 
            // maxHeartRate
            // 
            this.maxHeartRate.Location = new System.Drawing.Point(37, 122);
            this.maxHeartRate.Name = "maxHeartRate";
            this.maxHeartRate.Size = new System.Drawing.Size(165, 20);
            this.maxHeartRate.TabIndex = 1;
            // 
            // functionalThresholdPower
            // 
            this.functionalThresholdPower.Location = new System.Drawing.Point(37, 57);
            this.functionalThresholdPower.Name = "functionalThresholdPower";
            this.functionalThresholdPower.Size = new System.Drawing.Size(165, 20);
            this.functionalThresholdPower.TabIndex = 2;
            // 
            // functionalThresholdPowerLbl
            // 
            this.functionalThresholdPowerLbl.AutoSize = true;
            this.functionalThresholdPowerLbl.Location = new System.Drawing.Point(34, 36);
            this.functionalThresholdPowerLbl.Name = "functionalThresholdPowerLbl";
            this.functionalThresholdPowerLbl.Size = new System.Drawing.Size(168, 13);
            this.functionalThresholdPowerLbl.TabIndex = 3;
            this.functionalThresholdPowerLbl.Text = "Functional Threshold Power (FTP)";
            // 
            // maxHeartRateLbl
            // 
            this.maxHeartRateLbl.AutoSize = true;
            this.maxHeartRateLbl.Location = new System.Drawing.Point(34, 100);
            this.maxHeartRateLbl.Name = "maxHeartRateLbl";
            this.maxHeartRateLbl.Size = new System.Drawing.Size(116, 13);
            this.maxHeartRateLbl.TabIndex = 4;
            this.maxHeartRateLbl.Text = "Max Heart Rate (MHR)";
            // 
            // User
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.maxHeartRateLbl);
            this.Controls.Add(this.functionalThresholdPowerLbl);
            this.Controls.Add(this.functionalThresholdPower);
            this.Controls.Add(this.maxHeartRate);
            this.Controls.Add(this.applyBtn);
            this.Name = "User";
            this.Text = "User Settings";
            this.Load += new System.EventHandler(this.User_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button applyBtn;
        public System.Windows.Forms.TextBox maxHeartRate;
        public System.Windows.Forms.TextBox functionalThresholdPower;
        private System.Windows.Forms.Label functionalThresholdPowerLbl;
        private System.Windows.Forms.Label maxHeartRateLbl;
    }
}