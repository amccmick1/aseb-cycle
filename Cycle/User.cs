﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cycle
{
    public partial class User : Form
    {
        public User()
        {
            InitializeComponent();
        }

        private void applyBtn_Click(object sender, EventArgs e)
        {
            Cycle.instance.ftp = int.Parse(functionalThresholdPower.Text);
            Cycle.instance.mhr = int.Parse(maxHeartRate.Text);
            Cycle.instance.plot("linegraph");
        }

        private void User_Load(object sender, EventArgs e)
        {
            functionalThresholdPower.Text = Cycle.instance.ftp.ToString();
            maxHeartRate.Text = Cycle.instance.mhr.ToString();
        }
    }
}
