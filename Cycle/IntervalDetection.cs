﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cycle
{
    public partial class IntervalDetection : Form
    { 
        public IntervalDetection()
        {
            InitializeComponent();
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Cycle.instance.threshold = int.Parse(threshold.Text);
                Cycle.instance.tolerance = int.Parse(tolerance.Text);
                Cycle.instance.least_interval_size = int.Parse(leastIntervalDistance.Text);
                Cycle.instance.end_peak_value_minus_tolerance_start = int.Parse(endPeakToleranceStart.Text);
                Cycle.instance.end_peak_value_minus_tolerance_end = int.Parse(endPeakToleranceEnd.Text);
                
            }
            catch { }

            Cycle.instance.interval = null;
            Cycle.instance.manualintervalList = null;
            Cycle.instance.plot("linegraph");
            Cycle.instance.populateRightPanel();
        }

        private void IntervalDetection_Load(object sender, EventArgs e)
        {
            threshold.Text = Cycle.instance.threshold.ToString();
            tolerance.Text = Cycle.instance.tolerance.ToString();
            leastIntervalDistance.Text = Cycle.instance.least_interval_size.ToString();
            endPeakToleranceStart.Text = Cycle.instance.end_peak_value_minus_tolerance_start.ToString();
            endPeakToleranceEnd.Text = Cycle.instance.end_peak_value_minus_tolerance_end.ToString();
            Cycle.instance.intervals_enabled = true;
            Cycle.instance.intervalsToolStripMenuItem.Checked = true;
        }
    }
}
