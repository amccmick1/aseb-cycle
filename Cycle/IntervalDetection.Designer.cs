﻿namespace Cycle
{
    partial class IntervalDetection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.thresholdLbl = new System.Windows.Forms.Label();
            this.threshold = new System.Windows.Forms.TextBox();
            this.toleranceLbl = new System.Windows.Forms.Label();
            this.tolerance = new System.Windows.Forms.TextBox();
            this.leastIntervalDistanceLbl = new System.Windows.Forms.Label();
            this.leastIntervalDistance = new System.Windows.Forms.TextBox();
            this.endPeakToleranceEndLbl = new System.Windows.Forms.Label();
            this.endPeakToleranceEnd = new System.Windows.Forms.TextBox();
            this.endPeakToleranceStartLbl = new System.Windows.Forms.Label();
            this.endPeakToleranceStart = new System.Windows.Forms.TextBox();
            this.updateBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // thresholdLbl
            // 
            this.thresholdLbl.AutoSize = true;
            this.thresholdLbl.Location = new System.Drawing.Point(113, 89);
            this.thresholdLbl.Name = "thresholdLbl";
            this.thresholdLbl.Size = new System.Drawing.Size(54, 13);
            this.thresholdLbl.TabIndex = 3;
            this.thresholdLbl.Text = "Threshold";
            // 
            // threshold
            // 
            this.threshold.Location = new System.Drawing.Point(116, 109);
            this.threshold.Name = "threshold";
            this.threshold.Size = new System.Drawing.Size(148, 20);
            this.threshold.TabIndex = 2;
            this.threshold.Text = "0";
            // 
            // toleranceLbl
            // 
            this.toleranceLbl.AutoSize = true;
            this.toleranceLbl.Location = new System.Drawing.Point(113, 150);
            this.toleranceLbl.Name = "toleranceLbl";
            this.toleranceLbl.Size = new System.Drawing.Size(55, 13);
            this.toleranceLbl.TabIndex = 5;
            this.toleranceLbl.Text = "Tolerance";
            // 
            // tolerance
            // 
            this.tolerance.Location = new System.Drawing.Point(116, 170);
            this.tolerance.Name = "tolerance";
            this.tolerance.Size = new System.Drawing.Size(148, 20);
            this.tolerance.TabIndex = 4;
            this.tolerance.Text = "0";
            // 
            // leastIntervalDistanceLbl
            // 
            this.leastIntervalDistanceLbl.AutoSize = true;
            this.leastIntervalDistanceLbl.Location = new System.Drawing.Point(113, 212);
            this.leastIntervalDistanceLbl.Name = "leastIntervalDistanceLbl";
            this.leastIntervalDistanceLbl.Size = new System.Drawing.Size(116, 13);
            this.leastIntervalDistanceLbl.TabIndex = 7;
            this.leastIntervalDistanceLbl.Text = "Least Interval Distance";
            // 
            // leastIntervalDistance
            // 
            this.leastIntervalDistance.Location = new System.Drawing.Point(116, 232);
            this.leastIntervalDistance.Name = "leastIntervalDistance";
            this.leastIntervalDistance.Size = new System.Drawing.Size(148, 20);
            this.leastIntervalDistance.TabIndex = 6;
            this.leastIntervalDistance.Text = "0";
            // 
            // endPeakToleranceEndLbl
            // 
            this.endPeakToleranceEndLbl.AutoSize = true;
            this.endPeakToleranceEndLbl.Location = new System.Drawing.Point(304, 150);
            this.endPeakToleranceEndLbl.Name = "endPeakToleranceEndLbl";
            this.endPeakToleranceEndLbl.Size = new System.Drawing.Size(158, 13);
            this.endPeakToleranceEndLbl.TabIndex = 11;
            this.endPeakToleranceEndLbl.Text = "End Peak Minus Tolerance End";
            // 
            // endPeakToleranceEnd
            // 
            this.endPeakToleranceEnd.Location = new System.Drawing.Point(307, 170);
            this.endPeakToleranceEnd.Name = "endPeakToleranceEnd";
            this.endPeakToleranceEnd.Size = new System.Drawing.Size(148, 20);
            this.endPeakToleranceEnd.TabIndex = 10;
            this.endPeakToleranceEnd.Text = "0";
            // 
            // endPeakToleranceStartLbl
            // 
            this.endPeakToleranceStartLbl.AutoSize = true;
            this.endPeakToleranceStartLbl.Location = new System.Drawing.Point(304, 89);
            this.endPeakToleranceStartLbl.Name = "endPeakToleranceStartLbl";
            this.endPeakToleranceStartLbl.Size = new System.Drawing.Size(161, 13);
            this.endPeakToleranceStartLbl.TabIndex = 9;
            this.endPeakToleranceStartLbl.Text = "End Peak Minus Tolerance Start";
            // 
            // endPeakToleranceStart
            // 
            this.endPeakToleranceStart.Location = new System.Drawing.Point(307, 109);
            this.endPeakToleranceStart.Name = "endPeakToleranceStart";
            this.endPeakToleranceStart.Size = new System.Drawing.Size(148, 20);
            this.endPeakToleranceStart.TabIndex = 8;
            this.endPeakToleranceStart.Text = "0";
            // 
            // updateBtn
            // 
            this.updateBtn.Location = new System.Drawing.Point(337, 229);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(75, 23);
            this.updateBtn.TabIndex = 12;
            this.updateBtn.Text = "Apply";
            this.updateBtn.UseVisualStyleBackColor = true;
            this.updateBtn.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // IntervalDetection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 421);
            this.Controls.Add(this.updateBtn);
            this.Controls.Add(this.endPeakToleranceEndLbl);
            this.Controls.Add(this.endPeakToleranceEnd);
            this.Controls.Add(this.endPeakToleranceStartLbl);
            this.Controls.Add(this.endPeakToleranceStart);
            this.Controls.Add(this.leastIntervalDistanceLbl);
            this.Controls.Add(this.leastIntervalDistance);
            this.Controls.Add(this.toleranceLbl);
            this.Controls.Add(this.tolerance);
            this.Controls.Add(this.thresholdLbl);
            this.Controls.Add(this.threshold);
            this.Name = "IntervalDetection";
            this.Text = "Interval Detection Settings";
            this.Load += new System.EventHandler(this.IntervalDetection_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label thresholdLbl;
        private System.Windows.Forms.TextBox threshold;
        private System.Windows.Forms.Label toleranceLbl;
        private System.Windows.Forms.TextBox tolerance;
        private System.Windows.Forms.Label leastIntervalDistanceLbl;
        private System.Windows.Forms.TextBox leastIntervalDistance;
        private System.Windows.Forms.Label endPeakToleranceEndLbl;
        private System.Windows.Forms.TextBox endPeakToleranceEnd;
        private System.Windows.Forms.Label endPeakToleranceStartLbl;
        private System.Windows.Forms.TextBox endPeakToleranceStart;
        private System.Windows.Forms.Button updateBtn;
    }
}