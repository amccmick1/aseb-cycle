﻿namespace Cycle
{
    partial class Cycle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.zgc = new ZedGraph.ZedGraphControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importCycleDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoDetectIntervalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altitudeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.powerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.heartRateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pedalIndexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leftRightBalanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.heartRateZonesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.powerZonesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.intervalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeChartTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.intensityFactorLbl = new System.Windows.Forms.Label();
            this.normalisedPowerLbl = new System.Windows.Forms.Label();
            this.NP = new System.Windows.Forms.TextBox();
            this.IF = new System.Windows.Forms.TextBox();
            this.trainingStressScoreLbl = new System.Windows.Forms.Label();
            this.TSS = new System.Windows.Forms.TextBox();
            this.rightPanelTitleLbl = new System.Windows.Forms.Label();
            this.leftBtn = new System.Windows.Forms.Button();
            this.rightBtn = new System.Windows.Forms.Button();
            this.maximumAltitudeLbl = new System.Windows.Forms.Label();
            this.maximumAltitude = new System.Windows.Forms.TextBox();
            this.averageAltitudeLbl = new System.Windows.Forms.Label();
            this.averageAltitude = new System.Windows.Forms.TextBox();
            this.maximumPowerLbl = new System.Windows.Forms.Label();
            this.maximumPower = new System.Windows.Forms.TextBox();
            this.averagePowerLbl = new System.Windows.Forms.Label();
            this.averagePower = new System.Windows.Forms.TextBox();
            this.minimumHeartRateLbl = new System.Windows.Forms.Label();
            this.minimumHeartRate = new System.Windows.Forms.TextBox();
            this.maximumHeartRateLbl = new System.Windows.Forms.Label();
            this.maximumHeartRate = new System.Windows.Forms.TextBox();
            this.averageHeartRateLbl = new System.Windows.Forms.Label();
            this.averageHeartRate = new System.Windows.Forms.TextBox();
            this.maximumSpeedLbl = new System.Windows.Forms.Label();
            this.maximumSpeed = new System.Windows.Forms.TextBox();
            this.averageSpeedLbl = new System.Windows.Forms.Label();
            this.averageSpeed = new System.Windows.Forms.TextBox();
            this.totalDistanceLbl = new System.Windows.Forms.Label();
            this.totalDistance = new System.Windows.Forms.TextBox();
            this.openCalendarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // zgc
            // 
            this.zgc.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.zgc.Dock = System.Windows.Forms.DockStyle.Left;
            this.zgc.EditButtons = System.Windows.Forms.MouseButtons.Left;
            this.zgc.Location = new System.Drawing.Point(0, 24);
            this.zgc.Name = "zgc";
            this.zgc.PanModifierKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.None)));
            this.zgc.ScrollGrace = 0D;
            this.zgc.ScrollMaxX = 0D;
            this.zgc.ScrollMaxY = 0D;
            this.zgc.ScrollMaxY2 = 0D;
            this.zgc.ScrollMinX = 0D;
            this.zgc.ScrollMinY = 0D;
            this.zgc.ScrollMinY2 = 0D;
            this.zgc.Size = new System.Drawing.Size(1555, 800);
            this.zgc.TabIndex = 1;
            this.zgc.Visible = false;
            this.zgc.MouseDownEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zgc_MouseDownEvent);
            this.zgc.MouseUpEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zgc_MouseUpEvent);
            this.zgc.MouseMoveEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.zgc_MouseMoveEvent);
            this.zgc.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.zgc_MouseDoubleClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.dataToolStripMenuItem,
            this.chartToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1754, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importCycleDataToolStripMenuItem,
            this.openCalendarToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // importCycleDataToolStripMenuItem
            // 
            this.importCycleDataToolStripMenuItem.Name = "importCycleDataToolStripMenuItem";
            this.importCycleDataToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.importCycleDataToolStripMenuItem.Text = "Import Cycle Data";
            this.importCycleDataToolStripMenuItem.Click += new System.EventHandler(this.importCycleDataToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoDetectIntervalsToolStripMenuItem,
            this.userSettingsToolStripMenuItem});
            this.editToolStripMenuItem.Enabled = false;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // autoDetectIntervalsToolStripMenuItem
            // 
            this.autoDetectIntervalsToolStripMenuItem.Name = "autoDetectIntervalsToolStripMenuItem";
            this.autoDetectIntervalsToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.autoDetectIntervalsToolStripMenuItem.Text = "Auto-Detect Interval Settings";
            this.autoDetectIntervalsToolStripMenuItem.Click += new System.EventHandler(this.autoDetectIntervalsToolStripMenuItem_Click);
            // 
            // userSettingsToolStripMenuItem
            // 
            this.userSettingsToolStripMenuItem.Name = "userSettingsToolStripMenuItem";
            this.userSettingsToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.userSettingsToolStripMenuItem.Text = "User Settings";
            this.userSettingsToolStripMenuItem.Click += new System.EventHandler(this.userSettingsToolStripMenuItem_Click);
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.speedToolStripMenuItem,
            this.cadenceToolStripMenuItem,
            this.altitudeToolStripMenuItem,
            this.powerToolStripMenuItem,
            this.heartRateToolStripMenuItem,
            this.pedalIndexToolStripMenuItem,
            this.leftRightBalanceToolStripMenuItem,
            this.toolStripSeparator1,
            this.heartRateZonesToolStripMenuItem,
            this.powerZonesToolStripMenuItem,
            this.toolStripSeparator2,
            this.intervalsToolStripMenuItem});
            this.dataToolStripMenuItem.Enabled = false;
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.dataToolStripMenuItem.Text = "Data";
            // 
            // speedToolStripMenuItem
            // 
            this.speedToolStripMenuItem.Checked = true;
            this.speedToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.speedToolStripMenuItem.Name = "speedToolStripMenuItem";
            this.speedToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.speedToolStripMenuItem.Text = "Speed";
            this.speedToolStripMenuItem.Click += new System.EventHandler(this.speedToolStripMenuItem_Click);
            // 
            // cadenceToolStripMenuItem
            // 
            this.cadenceToolStripMenuItem.Checked = true;
            this.cadenceToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cadenceToolStripMenuItem.Name = "cadenceToolStripMenuItem";
            this.cadenceToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.cadenceToolStripMenuItem.Text = "Cadence";
            this.cadenceToolStripMenuItem.Click += new System.EventHandler(this.cadenceToolStripMenuItem_Click);
            // 
            // altitudeToolStripMenuItem
            // 
            this.altitudeToolStripMenuItem.Checked = true;
            this.altitudeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.altitudeToolStripMenuItem.Name = "altitudeToolStripMenuItem";
            this.altitudeToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.altitudeToolStripMenuItem.Text = "Altitude";
            this.altitudeToolStripMenuItem.Click += new System.EventHandler(this.altitudeToolStripMenuItem_Click);
            // 
            // powerToolStripMenuItem
            // 
            this.powerToolStripMenuItem.Checked = true;
            this.powerToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.powerToolStripMenuItem.Name = "powerToolStripMenuItem";
            this.powerToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.powerToolStripMenuItem.Text = "Power";
            this.powerToolStripMenuItem.Click += new System.EventHandler(this.powerToolStripMenuItem_Click);
            // 
            // heartRateToolStripMenuItem
            // 
            this.heartRateToolStripMenuItem.Checked = true;
            this.heartRateToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.heartRateToolStripMenuItem.Name = "heartRateToolStripMenuItem";
            this.heartRateToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.heartRateToolStripMenuItem.Text = "Heart Rate";
            this.heartRateToolStripMenuItem.Click += new System.EventHandler(this.heartRateToolStripMenuItem_Click);
            // 
            // pedalIndexToolStripMenuItem
            // 
            this.pedalIndexToolStripMenuItem.Checked = true;
            this.pedalIndexToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.pedalIndexToolStripMenuItem.Name = "pedalIndexToolStripMenuItem";
            this.pedalIndexToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.pedalIndexToolStripMenuItem.Text = "Pedal Index";
            this.pedalIndexToolStripMenuItem.Click += new System.EventHandler(this.pedalIndexToolStripMenuItem_Click);
            // 
            // leftRightBalanceToolStripMenuItem
            // 
            this.leftRightBalanceToolStripMenuItem.Checked = true;
            this.leftRightBalanceToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.leftRightBalanceToolStripMenuItem.Name = "leftRightBalanceToolStripMenuItem";
            this.leftRightBalanceToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.leftRightBalanceToolStripMenuItem.Text = "Left-Right-Balance";
            this.leftRightBalanceToolStripMenuItem.Click += new System.EventHandler(this.leftRightBalanceToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(170, 6);
            // 
            // heartRateZonesToolStripMenuItem
            // 
            this.heartRateZonesToolStripMenuItem.Name = "heartRateZonesToolStripMenuItem";
            this.heartRateZonesToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.heartRateZonesToolStripMenuItem.Text = "Heart Rate Zones";
            this.heartRateZonesToolStripMenuItem.Click += new System.EventHandler(this.heartRateZonesToolStripMenuItem_Click);
            // 
            // powerZonesToolStripMenuItem
            // 
            this.powerZonesToolStripMenuItem.Name = "powerZonesToolStripMenuItem";
            this.powerZonesToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.powerZonesToolStripMenuItem.Text = "Power Zones";
            this.powerZonesToolStripMenuItem.Click += new System.EventHandler(this.powerZonesToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(170, 6);
            // 
            // intervalsToolStripMenuItem
            // 
            this.intervalsToolStripMenuItem.Name = "intervalsToolStripMenuItem";
            this.intervalsToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.intervalsToolStripMenuItem.Text = "Intervals";
            this.intervalsToolStripMenuItem.Click += new System.EventHandler(this.intervalsToolStripMenuItem_Click);
            // 
            // chartToolStripMenuItem
            // 
            this.chartToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeChartTypeToolStripMenuItem});
            this.chartToolStripMenuItem.Enabled = false;
            this.chartToolStripMenuItem.Name = "chartToolStripMenuItem";
            this.chartToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.chartToolStripMenuItem.Text = "Chart";
            // 
            // changeChartTypeToolStripMenuItem
            // 
            this.changeChartTypeToolStripMenuItem.Name = "changeChartTypeToolStripMenuItem";
            this.changeChartTypeToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.changeChartTypeToolStripMenuItem.Text = "Switch to Pie Chart";
            this.changeChartTypeToolStripMenuItem.Click += new System.EventHandler(this.changeChartTypeToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.intensityFactorLbl);
            this.panel1.Controls.Add(this.normalisedPowerLbl);
            this.panel1.Controls.Add(this.NP);
            this.panel1.Controls.Add(this.IF);
            this.panel1.Controls.Add(this.trainingStressScoreLbl);
            this.panel1.Controls.Add(this.TSS);
            this.panel1.Controls.Add(this.rightPanelTitleLbl);
            this.panel1.Controls.Add(this.leftBtn);
            this.panel1.Controls.Add(this.rightBtn);
            this.panel1.Controls.Add(this.maximumAltitudeLbl);
            this.panel1.Controls.Add(this.maximumAltitude);
            this.panel1.Controls.Add(this.averageAltitudeLbl);
            this.panel1.Controls.Add(this.averageAltitude);
            this.panel1.Controls.Add(this.maximumPowerLbl);
            this.panel1.Controls.Add(this.maximumPower);
            this.panel1.Controls.Add(this.averagePowerLbl);
            this.panel1.Controls.Add(this.averagePower);
            this.panel1.Controls.Add(this.minimumHeartRateLbl);
            this.panel1.Controls.Add(this.minimumHeartRate);
            this.panel1.Controls.Add(this.maximumHeartRateLbl);
            this.panel1.Controls.Add(this.maximumHeartRate);
            this.panel1.Controls.Add(this.averageHeartRateLbl);
            this.panel1.Controls.Add(this.averageHeartRate);
            this.panel1.Controls.Add(this.maximumSpeedLbl);
            this.panel1.Controls.Add(this.maximumSpeed);
            this.panel1.Controls.Add(this.averageSpeedLbl);
            this.panel1.Controls.Add(this.averageSpeed);
            this.panel1.Controls.Add(this.totalDistanceLbl);
            this.panel1.Controls.Add(this.totalDistance);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(1554, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 800);
            this.panel1.TabIndex = 7;
            // 
            // intensityFactorLbl
            // 
            this.intensityFactorLbl.AutoSize = true;
            this.intensityFactorLbl.Location = new System.Drawing.Point(25, 653);
            this.intensityFactorLbl.Name = "intensityFactorLbl";
            this.intensityFactorLbl.Size = new System.Drawing.Size(79, 13);
            this.intensityFactorLbl.TabIndex = 34;
            this.intensityFactorLbl.Text = "Intensity Factor";
            // 
            // normalisedPowerLbl
            // 
            this.normalisedPowerLbl.AutoSize = true;
            this.normalisedPowerLbl.Location = new System.Drawing.Point(25, 605);
            this.normalisedPowerLbl.Name = "normalisedPowerLbl";
            this.normalisedPowerLbl.Size = new System.Drawing.Size(92, 13);
            this.normalisedPowerLbl.TabIndex = 32;
            this.normalisedPowerLbl.Text = "Normalised Power";
            // 
            // NP
            // 
            this.NP.Location = new System.Drawing.Point(28, 623);
            this.NP.Name = "NP";
            this.NP.ReadOnly = true;
            this.NP.Size = new System.Drawing.Size(148, 20);
            this.NP.TabIndex = 31;
            // 
            // IF
            // 
            this.IF.Location = new System.Drawing.Point(28, 671);
            this.IF.Name = "IF";
            this.IF.ReadOnly = true;
            this.IF.Size = new System.Drawing.Size(148, 20);
            this.IF.TabIndex = 33;
            // 
            // trainingStressScoreLbl
            // 
            this.trainingStressScoreLbl.AutoSize = true;
            this.trainingStressScoreLbl.Location = new System.Drawing.Point(25, 702);
            this.trainingStressScoreLbl.Name = "trainingStressScoreLbl";
            this.trainingStressScoreLbl.Size = new System.Drawing.Size(108, 13);
            this.trainingStressScoreLbl.TabIndex = 30;
            this.trainingStressScoreLbl.Text = "Training Stress Score";
            // 
            // TSS
            // 
            this.TSS.Location = new System.Drawing.Point(28, 720);
            this.TSS.Name = "TSS";
            this.TSS.ReadOnly = true;
            this.TSS.Size = new System.Drawing.Size(148, 20);
            this.TSS.TabIndex = 29;
            // 
            // rightPanelTitleLbl
            // 
            this.rightPanelTitleLbl.AutoSize = true;
            this.rightPanelTitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.rightPanelTitleLbl.Location = new System.Drawing.Point(49, 14);
            this.rightPanelTitleLbl.Name = "rightPanelTitleLbl";
            this.rightPanelTitleLbl.Size = new System.Drawing.Size(107, 26);
            this.rightPanelTitleLbl.TabIndex = 22;
            this.rightPanelTitleLbl.Text = "Summary";
            // 
            // leftBtn
            // 
            this.leftBtn.Location = new System.Drawing.Point(28, 765);
            this.leftBtn.Name = "leftBtn";
            this.leftBtn.Size = new System.Drawing.Size(50, 23);
            this.leftBtn.TabIndex = 21;
            this.leftBtn.Text = "<-";
            this.leftBtn.UseVisualStyleBackColor = true;
            this.leftBtn.Visible = false;
            this.leftBtn.Click += new System.EventHandler(this.leftBtn_Click);
            // 
            // rightBtn
            // 
            this.rightBtn.Location = new System.Drawing.Point(126, 765);
            this.rightBtn.Name = "rightBtn";
            this.rightBtn.Size = new System.Drawing.Size(50, 23);
            this.rightBtn.TabIndex = 20;
            this.rightBtn.Text = "->";
            this.rightBtn.UseVisualStyleBackColor = true;
            this.rightBtn.Visible = false;
            this.rightBtn.Click += new System.EventHandler(this.rightBtn_Click);
            // 
            // maximumAltitudeLbl
            // 
            this.maximumAltitudeLbl.AutoSize = true;
            this.maximumAltitudeLbl.Location = new System.Drawing.Point(25, 548);
            this.maximumAltitudeLbl.Name = "maximumAltitudeLbl";
            this.maximumAltitudeLbl.Size = new System.Drawing.Size(89, 13);
            this.maximumAltitudeLbl.TabIndex = 19;
            this.maximumAltitudeLbl.Text = "Maximum Altitude";
            // 
            // maximumAltitude
            // 
            this.maximumAltitude.Location = new System.Drawing.Point(28, 566);
            this.maximumAltitude.Name = "maximumAltitude";
            this.maximumAltitude.ReadOnly = true;
            this.maximumAltitude.Size = new System.Drawing.Size(148, 20);
            this.maximumAltitude.TabIndex = 18;
            // 
            // averageAltitudeLbl
            // 
            this.averageAltitudeLbl.AutoSize = true;
            this.averageAltitudeLbl.Location = new System.Drawing.Point(25, 494);
            this.averageAltitudeLbl.Name = "averageAltitudeLbl";
            this.averageAltitudeLbl.Size = new System.Drawing.Size(87, 13);
            this.averageAltitudeLbl.TabIndex = 17;
            this.averageAltitudeLbl.Text = "Average Alititude";
            // 
            // averageAltitude
            // 
            this.averageAltitude.Location = new System.Drawing.Point(28, 512);
            this.averageAltitude.Name = "averageAltitude";
            this.averageAltitude.ReadOnly = true;
            this.averageAltitude.Size = new System.Drawing.Size(148, 20);
            this.averageAltitude.TabIndex = 16;
            // 
            // maximumPowerLbl
            // 
            this.maximumPowerLbl.AutoSize = true;
            this.maximumPowerLbl.Location = new System.Drawing.Point(25, 440);
            this.maximumPowerLbl.Name = "maximumPowerLbl";
            this.maximumPowerLbl.Size = new System.Drawing.Size(84, 13);
            this.maximumPowerLbl.TabIndex = 15;
            this.maximumPowerLbl.Text = "Maximum Power";
            // 
            // maximumPower
            // 
            this.maximumPower.Location = new System.Drawing.Point(28, 458);
            this.maximumPower.Name = "maximumPower";
            this.maximumPower.ReadOnly = true;
            this.maximumPower.Size = new System.Drawing.Size(148, 20);
            this.maximumPower.TabIndex = 14;
            // 
            // averagePowerLbl
            // 
            this.averagePowerLbl.AutoSize = true;
            this.averagePowerLbl.Location = new System.Drawing.Point(25, 385);
            this.averagePowerLbl.Name = "averagePowerLbl";
            this.averagePowerLbl.Size = new System.Drawing.Size(80, 13);
            this.averagePowerLbl.TabIndex = 13;
            this.averagePowerLbl.Text = "Average Power";
            // 
            // averagePower
            // 
            this.averagePower.Location = new System.Drawing.Point(28, 403);
            this.averagePower.Name = "averagePower";
            this.averagePower.ReadOnly = true;
            this.averagePower.Size = new System.Drawing.Size(148, 20);
            this.averagePower.TabIndex = 12;
            // 
            // minimumHeartRateLbl
            // 
            this.minimumHeartRateLbl.AutoSize = true;
            this.minimumHeartRateLbl.Location = new System.Drawing.Point(25, 330);
            this.minimumHeartRateLbl.Name = "minimumHeartRateLbl";
            this.minimumHeartRateLbl.Size = new System.Drawing.Size(103, 13);
            this.minimumHeartRateLbl.TabIndex = 11;
            this.minimumHeartRateLbl.Text = "Minimum Heart Rate";
            // 
            // minimumHeartRate
            // 
            this.minimumHeartRate.Location = new System.Drawing.Point(28, 348);
            this.minimumHeartRate.Name = "minimumHeartRate";
            this.minimumHeartRate.ReadOnly = true;
            this.minimumHeartRate.Size = new System.Drawing.Size(148, 20);
            this.minimumHeartRate.TabIndex = 10;
            // 
            // maximumHeartRateLbl
            // 
            this.maximumHeartRateLbl.AutoSize = true;
            this.maximumHeartRateLbl.Location = new System.Drawing.Point(25, 273);
            this.maximumHeartRateLbl.Name = "maximumHeartRateLbl";
            this.maximumHeartRateLbl.Size = new System.Drawing.Size(106, 13);
            this.maximumHeartRateLbl.TabIndex = 9;
            this.maximumHeartRateLbl.Text = "Maximum Heart Rate";
            // 
            // maximumHeartRate
            // 
            this.maximumHeartRate.Location = new System.Drawing.Point(28, 291);
            this.maximumHeartRate.Name = "maximumHeartRate";
            this.maximumHeartRate.ReadOnly = true;
            this.maximumHeartRate.Size = new System.Drawing.Size(148, 20);
            this.maximumHeartRate.TabIndex = 8;
            // 
            // averageHeartRateLbl
            // 
            this.averageHeartRateLbl.AutoSize = true;
            this.averageHeartRateLbl.Location = new System.Drawing.Point(25, 219);
            this.averageHeartRateLbl.Name = "averageHeartRateLbl";
            this.averageHeartRateLbl.Size = new System.Drawing.Size(102, 13);
            this.averageHeartRateLbl.TabIndex = 7;
            this.averageHeartRateLbl.Text = "Average Heart Rate";
            // 
            // averageHeartRate
            // 
            this.averageHeartRate.Location = new System.Drawing.Point(28, 237);
            this.averageHeartRate.Name = "averageHeartRate";
            this.averageHeartRate.ReadOnly = true;
            this.averageHeartRate.Size = new System.Drawing.Size(148, 20);
            this.averageHeartRate.TabIndex = 6;
            // 
            // maximumSpeedLbl
            // 
            this.maximumSpeedLbl.AutoSize = true;
            this.maximumSpeedLbl.Location = new System.Drawing.Point(25, 162);
            this.maximumSpeedLbl.Name = "maximumSpeedLbl";
            this.maximumSpeedLbl.Size = new System.Drawing.Size(85, 13);
            this.maximumSpeedLbl.TabIndex = 5;
            this.maximumSpeedLbl.Text = "Maximum Speed";
            // 
            // maximumSpeed
            // 
            this.maximumSpeed.Location = new System.Drawing.Point(28, 180);
            this.maximumSpeed.Name = "maximumSpeed";
            this.maximumSpeed.ReadOnly = true;
            this.maximumSpeed.Size = new System.Drawing.Size(148, 20);
            this.maximumSpeed.TabIndex = 4;
            // 
            // averageSpeedLbl
            // 
            this.averageSpeedLbl.AutoSize = true;
            this.averageSpeedLbl.Location = new System.Drawing.Point(25, 109);
            this.averageSpeedLbl.Name = "averageSpeedLbl";
            this.averageSpeedLbl.Size = new System.Drawing.Size(81, 13);
            this.averageSpeedLbl.TabIndex = 3;
            this.averageSpeedLbl.Text = "Average Speed";
            // 
            // averageSpeed
            // 
            this.averageSpeed.Location = new System.Drawing.Point(28, 127);
            this.averageSpeed.Name = "averageSpeed";
            this.averageSpeed.ReadOnly = true;
            this.averageSpeed.Size = new System.Drawing.Size(148, 20);
            this.averageSpeed.TabIndex = 2;
            // 
            // totalDistanceLbl
            // 
            this.totalDistanceLbl.AutoSize = true;
            this.totalDistanceLbl.Location = new System.Drawing.Point(25, 59);
            this.totalDistanceLbl.Name = "totalDistanceLbl";
            this.totalDistanceLbl.Size = new System.Drawing.Size(76, 13);
            this.totalDistanceLbl.TabIndex = 1;
            this.totalDistanceLbl.Text = "Total Distance";
            // 
            // totalDistance
            // 
            this.totalDistance.Location = new System.Drawing.Point(28, 77);
            this.totalDistance.Name = "totalDistance";
            this.totalDistance.ReadOnly = true;
            this.totalDistance.Size = new System.Drawing.Size(148, 20);
            this.totalDistance.TabIndex = 0;
            // 
            // openCalendarToolStripMenuItem
            // 
            this.openCalendarToolStripMenuItem.Name = "openCalendarToolStripMenuItem";
            this.openCalendarToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.openCalendarToolStripMenuItem.Text = "Import Cycle Directory";
            this.openCalendarToolStripMenuItem.Click += new System.EventHandler(this.openCalendarToolStripMenuItem_Click);
            // 
            // Cycle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(1754, 824);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.zgc);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Cycle";
            this.Text = "CSDPA";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Cycle_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importCycleDataToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox totalDistance;
        private System.Windows.Forms.Label totalDistanceLbl;
        private System.Windows.Forms.Label maximumAltitudeLbl;
        private System.Windows.Forms.TextBox maximumAltitude;
        private System.Windows.Forms.Label averageAltitudeLbl;
        private System.Windows.Forms.TextBox averageAltitude;
        private System.Windows.Forms.Label maximumPowerLbl;
        private System.Windows.Forms.TextBox maximumPower;
        private System.Windows.Forms.Label averagePowerLbl;
        private System.Windows.Forms.TextBox averagePower;
        private System.Windows.Forms.Label minimumHeartRateLbl;
        private System.Windows.Forms.TextBox minimumHeartRate;
        private System.Windows.Forms.Label maximumHeartRateLbl;
        private System.Windows.Forms.TextBox maximumHeartRate;
        private System.Windows.Forms.Label averageHeartRateLbl;
        private System.Windows.Forms.TextBox averageHeartRate;
        private System.Windows.Forms.Label maximumSpeedLbl;
        private System.Windows.Forms.TextBox maximumSpeed;
        private System.Windows.Forms.Label averageSpeedLbl;
        private System.Windows.Forms.TextBox averageSpeed;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoDetectIntervalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altitudeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem powerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem heartRateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedalIndexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leftRightBalanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.ToolStripMenuItem intervalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeChartTypeToolStripMenuItem;
        private System.Windows.Forms.Button leftBtn;
        private System.Windows.Forms.Button rightBtn;
        private System.Windows.Forms.Label rightPanelTitleLbl;
        private System.Windows.Forms.Label intensityFactorLbl;
        private System.Windows.Forms.Label normalisedPowerLbl;
        private System.Windows.Forms.TextBox NP;
        private System.Windows.Forms.TextBox IF;
        private System.Windows.Forms.Label trainingStressScoreLbl;
        private System.Windows.Forms.TextBox TSS;
        private System.Windows.Forms.ToolStripMenuItem heartRateZonesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem powerZonesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem userSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openCalendarToolStripMenuItem;

    }
}

