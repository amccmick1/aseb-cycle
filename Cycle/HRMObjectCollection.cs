﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cycle
{
    public class HRMObjectCollection
    {
        private List<HRMObject> HRMObjects = new List<HRMObject>();

        public HRMObject getObject(int i)
        {
            return this.HRMObjects[i];
        }

        public void add(HRMObject HRMObject)
        {
            this.HRMObjects.Add(HRMObject);
        }

        public void remove(HRMObject HRMObject)
        {
            this.HRMObjects.Remove(HRMObject);
        }

        public int getCount()
        {
            return this.HRMObjects.Count;
        }

        public void clear()
        {
            this.HRMObjects.Clear();
        }
    }
}
